import json

from flask import Flask, render_template, request, redirect, url_for
import sqlalchemy_wrapper
from sqlalchemy_wrapper import SQLAlchemy

from sqlalchemy import String, Column, and_
from sqlalchemy_wrapper import SQLAlchemy

db = SQLAlchemy('sqlite:///app.db')

app = Flask(__name__)


class User(db.Model):
    __tablename__ = "user"

    username = db.Column(String, primary_key =True)
    comment = db.Column(String)


db.create_all()


@app.route('/deletefromindexheader',methods=['POST'])
def deletefromindexheader():
    try:
        name=request.form['name']
        comment=request.form['comment']
        var1= db.query(User).filter(and_(User.username==name , User.comment==comment )).first()
        db.delete(var1)
        db.commit()
        return redirect(url_for('index'))
    except:
        return redirect(url_for('index'))


# @app.route('/renderdfh',methods=['GET'])
# def dfh():
#     return render_template('deletefromheader.html')
#


@app.route('/',methods=['GET','POST'])
def index():
    query = db.query(User).filter_by().all()
    # print query
    dicto = {}
    for i in query:
        name = (str(i.username))
        comment = (str(i.comment))
        dicto[name] = comment
    return render_template('index.html',dicto=dicto)


@app.route('/sign')
def sign():
    return render_template('sign.html')


@app.route('/process', methods=['POST'])
def process():
    name = request.form['name']
    comment = request.form['comment']
    print request.form
    s = User(username = name , comment = comment)
    db.add(s)
    db.commit()
    #print name,comment
    return redirect(url_for('index'))


@app.route('/delete',methods=['GET','POST'])
def delete():
     if request.method=="POST":
        name = request.form['kkk1']
        obj1= db.query(User).filter_by().all()
        for i in obj1:
            print i.username,i.comment
            if str(name).strip()==str(i.username).strip():
                print "xyz"
                db.delete(i)
                db.commit()

                return redirect(url_for('index'))
     return redirect(url_for('sign'))


@app.route('/home', methods=['GET', 'POST'])
def home():
    links = ['https://www.youtube.com', 'https://www.bing.com', 'https://www.python.org', 'https://www.enkato.com']
    return render_template('example.html', links=links)


if __name__ == '__main__':
    app.run(debug=True)
