import pytest
from flask_webtest import TestApp
from hamcrest import assert_that, is_

from app import app, db


class Testweb(object):

    @pytest.fixture(autouse=True)
    def setup(self):
        self.app = app
        self.db = db
        self.test_app = TestApp(self.app, db=db, use_session_scopes=True)

    def test_me(self):
        response = self.test_app.get("/sign")
        import pdb
        assert_that(response.status_code, is_(200))

        assert True