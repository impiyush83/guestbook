$(function() {
    $('#button1').click(function() {
        $.ajax({
            url: '/delete',
            data: $('form').serialize(),
            type: 'POST',
            success: function(response) {
                console.log(response);
            },
            error: function(error) {
                console.log(error);
            }
        });
    });
});